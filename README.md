# govTech
[![pipeline status](https://gitlab.com/v1shva/govtech_test/badges/main/pipeline.svg)](https://gitlab.com/v1shva/govtech_test/-/commits/main)
[![coverage report](https://gitlab.com/v1shva/govtech_test/badges/main/coverage.svg)](https://gitlab.com/v1shva/govtech_test/-/commits/main)
> 
## Getting Started


1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```
    cd path/to/govtech_test
    npm install
    ```

3. Start the app

    ```
    npm start
    ```    

    ```
    npm run start "path/to/dir" // you can provide a path to scan as a param
    ```
    

You can skip step 2 if you are not going to run any tests.

## Testing

Simply run `npm test` and all the tests in the `test/` directory will be run.