const { resolve } = require('path');
const { readdir } = require('fs').promises;
const fs = require('fs').promises;

module.exports.getFiles = getFiles;
module.exports.scanFile = scanFile;

async function* getFiles(dir) {
    const dirents = await readdir(dir, { withFileTypes: true });
    for (const dirent of dirents) {
        const res = resolve(dir, dirent.name);
        if (dirent.isDirectory()) {
            yield* getFiles(res);
        } else {
            yield res;
        }
    }
}

async function scanFile(path, string) {
    const data = await fs.readFile(path);
    if (data.toString().includes(string)) {
        return true;
    } else return false;
}

(async () => {
    let path;
    if (process.argv.length == 3) {
        path = process.argv.slice(2)[0];
    }
    for await (const f of getFiles((path ? path : './test'))) {
        if (await scanFile(f, "TODO")) console.log(f);
    }
})();



