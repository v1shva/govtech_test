const assert = require('assert');
const app = require('../app');
const path = require('path');
describe('Scan a file at a given path for given keyword.', function () {
    it('should scan the given file and return true if the given keyword is found.', async function () {
        const status = await app.scanFile('./test/test-file.txt', 'test');
        assert.strictEqual(status, true);
    });
    it('should scan the given file and return false if the given keyword is not found.', async function () {
        const status = await app.scanFile('./test/test-file.txt', 'todo');
        assert.strictEqual(status, false);
    });
    it('should throw error when file is not found.', async () => {
        await assert.rejects(
            async () => {
                try {
                    await app.scanFile('./path/not/found', 'test');
                } catch (e) {
                    throw new Error(e.message);
                }
            },
            {
                name: 'Error',
                message: (process.platform == "linux" ? "ENOENT: no such file or directory, open './path/not/found'" :
                    "ENOENT: no such file or directory, open '" + path.normalize(path.resolve('./') + "/path/not/found") + "'")
            }
        );
    });
});

describe('List files that has todo keyword.', function () {
    it('should list all the files in a given path iteratively.', async function () {
        const itr = await app.getFiles('./test');
        assert.strictEqual((await itr.next()).value, path.normalize(__dirname + '/app.test.js'));
        assert.strictEqual((await itr.next()).value, path.normalize(__dirname + '/test-file.txt'));
        assert.strictEqual((await itr.next()).value, path.normalize(__dirname + '/test-todo.js'));
        // assert.strictEqual(status, true);
    });
    it('should throw error when path is incorrect.', async () => {
        await assert.rejects(
            async () => {
                try {
                    const itr = await app.getFiles('./path/not/found');
                    (await itr.next()).value;
                } catch (e) {
                    throw new Error(e.message);
                }
            },
            {
                name: 'Error',
                message: (process.platform == "linux" ? "ENOENT: no such file or directory, scandir './path/not/found'" :
                    "ENOENT: no such file or directory, scandir '" + path.normalize(path.resolve('./') + "/path/not/found") + "'")
            }
        );
    });
});
